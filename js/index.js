




$("#modal2 #prev_btn").click(function() {
  $("#modal2 p#joke_area").text(joke_stack.getPrevious());
  $("#modal2 p#category_name").text(
    joke_stack.index + 1 + " out of 10 random jokes from " + joke_stack.category
  );
});

$("#modal2 #next_btn").click(function() {
  $("#modal2 p#joke_area").text(joke_stack.getNext());
  $("#modal2 p#category_name").text(
    joke_stack.index + 1 + " out of 10 jokes from " + joke_stack.category
  );
});

$("#modal2 #fav_btn").click(function() {
  var val = $("p#joke_area").text();
  if (fav_jokes.indexOf(val) != -1) {
    Materialize.toast("Already in favourites!", 1000);
    return;
  }
  addJokeToFavourites(val);
  Materialize.toast("Added to favourites!", 2000);
});

$("#modal1 #dropdown1").on("click", "li a", function(event) {
  var category_text = event.target.innerText;
  $.ajax({
    url: "https://api.chucknorris.io/jokes/random?category=" + category_text,
    success: function(data) {
      $("#modal2 #joke_area").text(data.value);
      joke_stack.fetchJokes(category_text);
      $("#modal2 p#category_name").text(
        "1 out of 10 random jokes from " + joke_stack.category
      );
      $("#modal2").modal("open");
    }
  });
});

  

 function get_filtered_jokes(words) {
    var filtered_jokes = []; // return variable

    /*At first fetch the all JSONs with selected words */
    for (var i = 0; i < words.length; i++)
      $.ajax({
        url: "https://api.chucknorris.io/jokes/search?query=" + words[i],
        success: function(data) {
          JSON_responses.push(data);
        },
        async: false
      });

    /* Filter only jokes that contains ALL the words */
    /* What is the better way to do this without being messy? */

    for (var j = 0; j < JSON_responses.length; j++)
      for (var k = 0; k < JSON_responses[j].result.length; k++) {
        var s = JSON_responses[j].result[k].value;
        if (match_all(s, words) && filtered_jokes.includes(s) == false) {
          filtered_jokes.push(s);
        }
      }

    return filtered_jokes;
  }


$("#search_btn").on('click',function(){
  $("#results_window .modal-content").text("");
  query_words = [];
  
  $(".chip").each(function() {
    var s = $(this)
      .contents()
      .get(0).nodeValue;
    query_words.push(s);
  });

   /* Getting results and adding them to DOM */
  var filtered = get_filtered_jokes(query_words);
  add_jokes(filtered);
  
  $("#results_window").modal("open");

  for (var i in query_words)
    highlight_words(query_words[i], $(".filter_jokes"));
});



jQuery(document).ready(function() {
  
  //Getting the categories via API and putting them as list items in DOM
    $.get("https://api.chucknorris.io/jokes/categories", function(data) {
      for (var i in data) {
        $("#modal1 #dropdown1").append("<li><a>" + data[i] + "</a></li>");
      }
    });
  jQuery(".modal").modal();
  $(".chips").material_chip();
  
 
});

  //click handler when user clicks on delete icon in favourites window
  $("#modal3 .modal-content").on("click", "p i", function(event) {
    
    event.target.parentNode.parentNode.removeChild(event.target.parentNode);
  });

fav_jokes = []; // favourite list
JSON_responses = []; //array for handling API calls
query_words = []; // array for storing words the jokes are being searched by

/* Helper function */
function highlight_words(keywords, element) {
  if (keywords) {
    var textNodes;
    keywords = keywords.replace(/\W/g, "");
    var str = keywords.split(" ");
    $(str).each(function() {
      var term = this;
      var textNodes = $(element)
        .contents()
        .filter(function() {
          return this.nodeType === 3;
        });
      textNodes.each(function() {
        var content = $(this).text();
        var regex = new RegExp(term, "gi");
        content = content.replace(
          regex,
          '<span class="highlight">' + term + "</span>"
        );
        $(this).replaceWith(content);
      });
    });
  }
}

/* We fetch 10jokes at once and handle it here */
joke_stack = {
  deck: [],
  category: "",
  push: function(arg) {
    this.deck.push(arg);
  },
  index: -1,
  getNext: function() {
    if (this.index == 9) {
      this.index = -1;
    }
    this.index++;
    return this.deck[this.index];
  },
  getPrevious: function() {
    if (this.index <= 0) this.index = 10; // Either we are in initial state, or user just hit 1st joke.
    this.index--;
    return this.deck[this.index];
  },
  emptyAll: function() {
    this.deck = [];
  },
  fetchJokes: function(category) {
    this.category = category;
    this.emptyAll();
    for (var i = 0; i < 10; i++)
      $.ajax({
        url: "https://api.chucknorris.io/jokes/random?category=" + category,
        success: function(data) {
          joke_stack.deck.push(data.value);
        }
      });
  }
};

/* Make joke show on favourites modal window */

function addJokeToFavourites(text) {
  fav_jokes.push(text);
  // add text with delete icon
  $(" <p>" + text + '<i class="material-icons">delete</i></p> ').appendTo(
    "#modal3 .modal-content"
  );}



  /* add jokes to results window after search */
  function add_jokes(jokes) {
    for (var i in jokes) {
      $('<p class ="filter_jokes">' + jokes[i] + "</p>").appendTo(
        "#results_window .modal-content"
      );
    }
  }

  /* Returns true when ALL words are included in string */
  function match_all(str, words) {
    for (var i = 0; i < words.length; i++)
      if (str.includes(words[i])) {
        continue;
      } else {
        return false;
      }

    return true;
  }
